#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

docker inspect $(docker ps -q) \
| jq -c '.[] | {name: .["Name"], ips: .["NetworkSettings"]["Networks"] | map(.IPAddress)} | [.name, .ips]'