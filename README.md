## Inspiration

https://www.reddit.com/r/selfhosted/comments/84xb7n/sharing_my_media_stack_dockercompose_file/


## Install Ansible

    sudo apt-add-repository ppa:ansible/ansible

    sudo apt-get update
    sudo apt-get -y install ansible


## Install Docker

    ansible-galaxy install nickjj.docker

    git clone git@gitlab.com:lucianofiandesio/personal-selfhosted.git

    cd personal-selfhosted/ansible

    sudo ansible-playbook -i hosts --connection=local docker.yaml

Moved the docker root folder on December 2018 to:

/home/dockeruser/docker/docker

using this script: https://sysinfo.io/ubuntu-16-04-move-docker-root/

## Install dockly

https://github.com/lirantal/dockly


## Traefik setup

- Copy the file 'traefik/traefik.toml' into `SELFHOSTED_ROOT/traefik`
- in `SELFHOSTED_ROOT/traefik` execute `touch acme.json`
- make sure both files are owned by root.
- also `sudo chmod 600 SELFHOSTED_ROOT/traefik/acme.json`

## Start/Stop

    sudo ./apps.sh up -d

    sudo ./apps.sh down

## Services

### Resilio sync

- Open firewall on port 55555

### NZBGET

- Open firewall on port 6789

### TTRSS

Export the database

`sudo docker exec {DB-CONTAINER} /usr/bin/mysqldump -u ttRss --password={PASSWORD} ttRssDb > ttrss-db.sql`

Import the database

`cat ttrss-db.sql | docker exec -i {DB-CONTAINER} /usr/bin/mysql -u ttRss --password={PASSWORD} ttRssDb`

### Deluge

- Open firewall port: 58946 (tcp/udp)

## Import database

Define a databse in `docker-compose.yml` like so:

    db:
      image: mysql
      container_name: mysql
      restart: always
      environment:
          - MYSQL_ROOT_PASSWORD=my-root-password
          - MYSQL_DATABASE=database-name
          - MYSQL_USER=database-user
          - MYSQL_PASSWORD=user-password

The database is created when the container is created. Run the following comand to import from a `mysqldump` file:

    sudo docker exec -i mysql  mysql -uroot -pmy-root-password database-name < ../database-name.sql

## Connect to running db

Get IP of running Mysql instance:

    ./container-ips.sh

Connect to db

    mysql -h 172.20.0.2 -u root -pctDVh7ccG5je23guSle

## FTPD configuration

Use this guide:
https://www.digitalocean.com/community/tutorials/how-to-set-up-vsftpd-for-a-user-s-directory-on-ubuntu-16-04

Use config file in 'ftpd' folder

## Access running container

  sudo docker-compose exec container_name /bin/bash

or

  sudo docker exec -i -t container_name /bin/bash

## Merge all download folders into one

    sudo apt-get install unionfs-fuse

    unionfs-fuse /folder1=RW:/folder2=RW /mount/point

Unmount:

    sudo umount /mount/point

Example:

    unionfs-fuse /home/koevet/selfhosted/deluge/downloads/=RO:/home/koevet/selfhosted/jdownloader-2/Downloads=RO:/home/koevet/selfhosted/nzbget/downloads/completed=RO -o allow_root -o allow_other /home/koevet/alldownloads/

## H5AI configuration

First create the unionfs mount (use above comand, note that `allow_root` is the key!)

Start h5ai `sudo docker-compose up -d h5ai`

## Assign folder to multiple users

https://superuser.com/questions/280994/give-write-permissions-to-multiple-users-on-a-folder-in-ubuntu


## Fix huginn database

### stop and rebuild huginn

  sudo docker-compose stop huginn

  sudo docker-compose rm huginn

  sudo docker-compose up -d --no-deps --build huginn


### get huginn id

  sudo docker ps

  sudo docker cp /home/koevet/personal-selfhosted/hug.sql 1bdc1b34bfef:/app

### get into huginn container

  sudo docker exec -i -t huginn /bin/bash

### re-import db

  mysql -uroot -ppassword huginn_production < hug.sql
