#!/usr/bin/env bash
BASE_DIR=$(cd "$(dirname "$0")"; pwd)

docker-compose \
   -f "$BASE_DIR/apps/traefik.yml" \
   -f "$BASE_DIR/apps/baikal.yml" \
   -f "$BASE_DIR/apps/resilio-sync.yml" \
   -f "$BASE_DIR/apps/nzbget.yml" \
   -f "$BASE_DIR/apps/hydra.yml" \
   -f "$BASE_DIR/apps/ttRss.yml" \
   -f "$BASE_DIR/apps/deluge.yml" \
   -f "$BASE_DIR/apps/huginn.yml" \
   -f "$BASE_DIR/apps/filerun.yml" \
   -f "$BASE_DIR/apps/bitwarden.yml" \
   "$@"
